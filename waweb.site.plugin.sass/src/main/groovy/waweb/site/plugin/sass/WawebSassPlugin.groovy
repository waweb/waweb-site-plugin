/**
 * 
 */
package waweb.site.plugin.sass

import static waweb.site.plugin.Constants.GROUP_NAME

import javax.inject.Inject

import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory

import waweb.site.plugin.SiteExtension
import waweb.site.plugin.SitePluginSpec
import waweb.site.plugin.WawebSitePlugin
import waweb.site.plugin.WebComponent

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class WawebSassPlugin implements SitePluginSpec {

    @Delegate
    private Project _target

    private Task _clean

    private Task _assemble

    @Override
    @Inject
    public ProviderFactory getProviderFactory() {}

    void apply(Project project) {
        _target = project

        //ensure base plugin is applied
        getPluginManager().apply(WawebSitePlugin)

        _clean = getTasks().getByName('clean')
        _assemble = getTasks().getByName('assembleSite')

        getExtensions().findByType(SiteExtension).components
                .all(this.&initializeComponentDefaults)
    }

    @Override
    void initializeComponentDefaults(final WebComponent component) {

        // Create a deferred File provider to resolve the default output dir
        final Provider<File> outputDir = providerFactory.provider {
            final File outputDir = getExtensions().findByType(SiteExtension).outputDir
            return outputDir?
                    new File(outputDir, 'css') :
                    getLayout().buildDirectory.dir("${GROUP_NAME}/${component.name}/css").get().asFile
        }

        final SassSourceSet sourceSet = component.extend(SassSourceSet, "sass", "SaSS Sources")
        sourceSet.srcDir("src/${component.name}/sass")
        sourceSet.filter.include("**/*.scss")
        sourceSet.setOutputDir(outputDir)

        // Construct a copy task to export resources to output dir
        final CompileSass compileSass = constructCompileSassTask(sourceSet)
        compileSass.mustRunAfter(_clean)

        // Set task to run in the assemble life-cycle
        _assemble.dependsOn(compileSass)
    }

    CompileSass constructCompileSassTask(final SassSourceSet sources) {
        final WebComponent component = sources.component
        final String componentName = (component.name == "main")?
                "" : component.name.capitalize()
        final String taskName = "assembleSite${componentName}Css"

        return getTasks().create(taskName, CompileSass) { CompileSass compileSass ->
            compileSass.group = GROUP_NAME
            compileSass.description = "Assemble CSS Outputs"
            compileSass.sources.set(sources)
        }
    }
}
