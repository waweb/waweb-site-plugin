/**
 * 
 */
package waweb.site.plugin.sass

import org.gradle.api.internal.file.FileCollectionFactory
import org.gradle.api.internal.file.collections.DirectoryFileTreeFactory
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.tasks.util.PatternSet
import org.gradle.internal.Factory

import waweb.site.plugin.DefaultSourceSet
import waweb.site.plugin.WebComponent

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class SassSourceSet extends DefaultSourceSet {

    final Property<Boolean> minify

    SassSourceSet(String name, 
            String displayName,
            Factory<PatternSet> patternSetFactory,
            FileCollectionFactory fileCollectionFactory,
            DirectoryFileTreeFactory directoryFileTreeFactory,
            ObjectFactory objectFactory,
            WebComponent component) {
        super(name, displayName, patternSetFactory, fileCollectionFactory, directoryFileTreeFactory, objectFactory, component)

        this.minify = objectFactory.property(Boolean)
        this.minify.convention(true)
    }
}
