/**
 * 
 */
package waweb.site.plugin.sass

import javax.inject.Inject

import org.gradle.api.DefaultTask
import org.gradle.api.file.FileVisitDetails
import org.gradle.api.file.RelativePath
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction

import com.vaadin.sass.internal.ScssStylesheet
import com.vaadin.sass.internal.resolver.FilesystemResolver

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class CompileSass extends DefaultTask {

    @Input
    final Property<String> encoding = objectFactory.property(String)

    @Input
    final Provider<Boolean> minify = providerFactory.provider {
        return sources.get().minify.get()
    }

    @InputFiles
    final Property<SassSourceSet> sources = objectFactory.property(SassSourceSet)

    @OutputDirectory
    final Provider<File> outputDir = providerFactory.provider {
        return sources.get().getOutputDir()
    }

    @Inject
    ProviderFactory getProviderFactory() {}

    @Inject
    ObjectFactory getObjectFactory() {}

    CompileSass() {
        this.encoding.convention(System.properties.get('file.encoding'))
    }

    @TaskAction
    void run() {

        final File outputDir = this.outputDir.get()
        outputDir.mkdirs();

        final boolean minify = this.minify.get()
        final String extension = minify? "min.css" : "css"
        final File srcDir = project.file('src/main/sass')
        this.sources.get().getSourceDirectories().asFileTree.visit { FileVisitDetails element  ->
            final RelativePath relativePath = element.getRelativePath()
            if(relativePath.isFile()) {
                final String basePath = relativePath.toString()
                        .take(relativePath.toString().lastIndexOf('.scss'))

                final File outputFile = new File(outputDir, "${basePath}.${extension}")
                outputFile.parentFile.mkdirs()

                final ScssStylesheet sass = ScssStylesheet.get(element.file.getAbsolutePath())
                sass.setFile(element.file)
                sass.setCharset(this.encoding.get())
                sass.addResolver(new FilesystemResolver(srcDir.getAbsolutePath()));
                sass.compile()

                outputFile.withWriter {
                    sass.write(it, minify)
                }
            }
        }
    }
}
