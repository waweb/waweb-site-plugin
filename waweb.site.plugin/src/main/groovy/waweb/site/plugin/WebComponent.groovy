/**
 * 
 */
package waweb.site.plugin

import javax.inject.Inject

import org.gradle.api.Named
import org.gradle.api.model.ObjectFactory
import org.gradle.api.plugins.ExtensionAware

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
abstract class WebComponent implements Named, ExtensionAware {

	private final SourceSetFactory sourceSetFactory

	private final String _name

	@Override
	public String getName() {
		return _name
	}

	final SourceSet resources

	@Inject
	ObjectFactory getObjectFactory() {}

	WebComponent(String name) {
		_name = name

		this.sourceSetFactory = objectFactory.newInstance(SourceSetFactory, this)
		this.resources = this.sourceSetFactory.sourceSet(DefaultSourceSet, 'resources', "Static Resources")

		this.metaClass = new ExpandoMetaClass(WebComponent, false, true)
		this.metaClass.initialize()
	}

	void resources(@DelegatesTo(SourceSet) Closure closure) {
		this.resources.with(closure)
	}

	/**
	 * Creates a new extension object and returns it. A new method is added using
	 * the same name as the extension to provide a configuration DSL
	 * @param <T>
	 * @param type
	 * @param name
	 * @param displayName
	 * @return the constructed extension object
	 */
	public <T extends SourceSet> T extend(Class<T> type, final String name, final String displayName) {

		// Add the extension object
		final T sourceSet = sourceSetFactory.sourceSet(type, name, displayName)
		getExtensions().add(name, sourceSet)

		// Create a configuration method of the same name
		((ExpandoMetaClass) this.metaClass).registerInstanceMethod(name) { Closure closure ->
			getExtensions().findByName(name).with(closure)
		}

		return sourceSet
	}
}
