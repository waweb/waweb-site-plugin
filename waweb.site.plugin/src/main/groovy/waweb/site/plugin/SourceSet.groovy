/**
 * 
 */
package waweb.site.plugin

import org.gradle.api.file.SourceDirectorySet

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
interface SourceSet extends SourceDirectorySet {
}
