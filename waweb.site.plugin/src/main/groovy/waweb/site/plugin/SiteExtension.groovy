/**
 * 
 */
package waweb.site.plugin

import javax.inject.Inject

import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.model.ObjectFactory

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class SiteExtension {

    final NamedDomainObjectContainer<WebComponent> components = objectFactory
            .domainObjectContainer(WebComponent)

    File outputDir

    @Inject
    ObjectFactory getObjectFactory() {}
}
