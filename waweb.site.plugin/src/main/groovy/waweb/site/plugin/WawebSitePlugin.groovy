/**
 * 
 */
package waweb.site.plugin

import static waweb.site.plugin.Constants.GROUP_NAME

import javax.inject.Inject

import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.plugins.BasePlugin
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.tasks.Copy

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class WawebSitePlugin implements SitePluginSpec {

    @Delegate
    Project _target

    Task _clean

    Task _assembleSite

    @Override
    @Inject
    ProviderFactory getProviderFactory() {}

    @Override
    void apply(Project project) {
        _target = project

        getPluginManager().apply(BasePlugin)

        _clean = getTasks().getByName('clean')
        _assembleSite = getTasks().create('assembleSite', Task) { Task task ->
            task.group = GROUP_NAME
            task.description = "Assemble Website"
            task.mustRunAfter(_clean)
        }
        getTasks().getByName('assemble').dependsOn(_assembleSite)

        final SiteExtension site = getExtensions().create(GROUP_NAME, SiteExtension)
        site.components.all(this.&initializeComponentDefaults)

        site.components.maybeCreate('main')
    }

    @Override
    void initializeComponentDefaults(final WebComponent component) {

        // Create a deferred File provider to resolve the default output dir
        final Provider<File> outputDir = providerFactory.provider {
            getExtensions().findByType(SiteExtension).outputDir ?:
                    getLayout().buildDirectory.dir("${GROUP_NAME}/${component.name}").get().asFile
        }

        // Set resources defaults
        component.resources.srcDir("src/${component.name}/webapp")
        component.resources.filter.include("**/*")
        component.resources.filter.exclude("META-INF/*")
        component.resources.filter.exclude("WEB-INF/*")
        component.resources.setOutputDir(outputDir)

        // Construct a copy task to export resources to output dir
        final Copy assembleResources = constructCopyResourcesTask(component)
        assembleResources.mustRunAfter(_clean)

        // Set task to run in the assemble life-cycle
        _assembleSite.dependsOn(assembleResources)
    }

    Copy constructCopyResourcesTask(final WebComponent component) {
        final String componentName = (component.name == "main")?
                "" : component.name.capitalize()
        final String taskName = "assembleSite${componentName}Resources"

        return getTasks().create(taskName, Copy) { Copy copy ->
            copy.group = GROUP_NAME
            copy.description = "Assemble Static Resources"
            copy.includeEmptyDirs = false
            copy.from(component.resources)
            copy.into { component.resources.getOutputDir() }
        }
    }
}
