/**
 * 
 */
package waweb.site.plugin

import javax.inject.Inject

import org.gradle.api.internal.file.FileCollectionFactory
import org.gradle.api.internal.file.FileResolver
import org.gradle.api.internal.file.collections.DirectoryFileTreeFactory
import org.gradle.api.model.ObjectFactory

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class SourceSetFactory {

    private final WebComponent component

    private final FileResolver fileResolver

    private final DirectoryFileTreeFactory directoryFileTreeFactory

    private final FileCollectionFactory fileCollectionFactory

    private final ObjectFactory objectFatory

    @Inject
    public SourceSetFactory(WebComponent component, FileResolver fileResolver, DirectoryFileTreeFactory directoryFileTreeFactory, FileCollectionFactory fileCollectionFactory, ObjectFactory objectFactory) {
        this.component = component
        this.fileResolver = fileResolver
        this.fileCollectionFactory = fileCollectionFactory
        this.directoryFileTreeFactory = directoryFileTreeFactory
        this.objectFatory = objectFactory
    }

    public <T extends SourceSet> T  sourceSet(Class<T> type, final String name, final String displayName) {
        return type.newInstance(name,
                displayName,
                fileResolver.getPatternSetFactory(),
                fileCollectionFactory,
                directoryFileTreeFactory,
                objectFatory,
                component)
    }
}
