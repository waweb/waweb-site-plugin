/**
 * 
 */
package waweb.site.plugin

import javax.inject.Inject

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.provider.ProviderFactory

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
interface SitePluginSpec extends Plugin<Project> {

    /**
     * @param component
     */
    void initializeComponentDefaults(final WebComponent component)

    @Inject
    ProviderFactory getProviderFactory()
}
