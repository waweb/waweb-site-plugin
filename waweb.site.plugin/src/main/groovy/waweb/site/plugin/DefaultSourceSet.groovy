/**
 * 
 */
package waweb.site.plugin

import org.gradle.api.internal.file.DefaultSourceDirectorySet
import org.gradle.api.internal.file.FileCollectionFactory
import org.gradle.api.internal.file.collections.DirectoryFileTreeFactory
import org.gradle.api.model.ObjectFactory
import org.gradle.api.tasks.util.PatternSet
import org.gradle.internal.Factory

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
public class DefaultSourceSet extends DefaultSourceDirectorySet implements SourceSet {

    final WebComponent component

    /**
     * @param name
     * @param displayName
     * @param patternSetFactory
     * @param fileCollectionFactory
     * @param directoryFileTreeFactory
     * @param objectFactory
     */
    public DefaultSourceSet(String name,
            String displayName,
            Factory<PatternSet> patternSetFactory,
            FileCollectionFactory fileCollectionFactory,
            DirectoryFileTreeFactory directoryFileTreeFactory,
            ObjectFactory objectFactory,
            WebComponent component) {
        super(name, displayName, patternSetFactory, fileCollectionFactory, directoryFileTreeFactory, objectFactory)

        this.component = component
    }
}
