/**
 * 
 */
package waweb.site.plugin.util

import javax.inject.Inject

import org.gradle.api.Named
import org.gradle.api.NamedDomainObjectCollection
import org.gradle.api.model.ObjectFactory

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class NamedDomainObjectFactory<T extends Named> {
        
        private final NamedDomainObjectCollection<T> container
        
        private final Class<T> defaultType
        
        private final ObjectFactory objectFactory
    
        NamedDomainObjectFactory(ObjectFactory objectFactory, NamedDomainObjectCollection<T> container, Class<T> defaultType = null) {
            this.objectFactory = objectFactory
            this.container = container
            this.defaultType = defaultType
        }
    
        void createOrConfigureAll(Closure closure) {
            closure.setResolveStrategy(Closure.DELEGATE_FIRST)
            closure.setDelegate(this)
            closure.call()
        }
    
        Object methodMissing(String name, Object args) {
            //println "NamedDomainObjectFactory.methodMissing(${name}, ${args})"
            if(((List) args).size() == 0) {
                // Create default instance of default type
                if(!defaultType) throw new RuntimeException("Default Type Required.")
                container.add(objectFactory.newInstance(defaultType, name))
            }
            else if(((List) args).size() == 1) {
                if(args.getAt(0) instanceof Closure) {
                    // Configure or create an instance with the provided closure
                    final T instance = container.findByName(name)
                    if(instance) {
                        // Configure instance if it exists
                        instance.with(closure)
                    }
                    else {
                        // Otherwise Create and configure instance of default type
                        if(!defaultType) throw new RuntimeException("Default Type Required.")
                        container.add(objectFactory.newInstance(defaultType, name).tap(args.getAt(0)))
                    }
                }
                else {
                    // Otherwise create default instance of specified type
                    final Class<T> type = args.getAt(0)
                    container.add(objectFactory.newInstance(type, name))
                }
            }
            else if(((List) args).size() == 2) {
                // Create and configure instance of specified type
                final Class<T> type = args.getAt(0)
                final Closure config = args.getAt(1)
                container.add(objectFactory.newInstance(type, name).tap(config))
            }
        }
}
