/**
 * 
 */
package waweb.site.plugin.js

import javax.inject.Inject

import org.gradle.api.DefaultTask
import org.gradle.api.file.CopySpec
import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

import com.google.javascript.jscomp.SourceMap

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class AssembleJs extends DefaultTask {

    @Internal
    final Provider<ClosureCompilerExtension> compiler = providerFactory.provider {
        return this.sources.get().getCompiler()
    }

    @Input
    final Property<String> encoding = objectFactory.property(String)

    @Input
    final Provider<String> warningLevel = providerFactory.provider {
        return this.compiler.get().getWarningLevel()
    }

    @Input
    final Provider<String> compilationLevel = providerFactory.provider {
        return this.compiler.get().getCompilationLevel()
    }

    @Input
    final Provider<String> languageIn = providerFactory.provider {
        return this.compiler.get().getLanguageIn()
    }

    @Input
    final Provider<String> languageOut = providerFactory.provider {
        return this.compiler.get().getLanguageOut()
    }

    @InputFiles
    @Optional
    final Property<JavaScriptSourceSet> sources = objectFactory.property(JavaScriptSourceSet)

    @InputFiles
    @Optional
    final Property<SourceDirectorySet> externs = objectFactory.property(SourceDirectorySet)

    @OutputDirectory
    final Provider<File> outputDir = providerFactory.provider {
        return sources.get().getOutputDir()
    }

    @Input
    final Provider<Boolean> exportSources = providerFactory.provider {
        return sources.get().getExportSources()
    }

    @OutputFile
    final Provider<File> minifiedOutput = providerFactory.provider {
        return compiler.get().getMinifiedOutput() ?:
                new File(outputDir.get(), "${this.sources.get().outputFileName}.min.js")
    }

    @OutputFile
    final Provider<File> sourceMapOutput = providerFactory.provider {
        return compiler.get().getSourceMapOutput() ?:
                new File(outputDir.get(), "${this.sources.get().outputFileName}.sourcemap.json")
    }

    @Inject
    ProviderFactory getProviderFactory() {}

    @Inject
    ObjectFactory getObjectFactory() {}

    AssembleJs() {
        this.encoding.convention(System.properties.get('file.encoding'))
    }

    @TaskAction
    void run() {
        final File outputDir = this.outputDir.get()
        final ClosureCompilerExtension closureSpec = this.compiler.get()
        logger.info("Assembling JavaScript output in ${outputDir}")

        exportSources(outputDir)
        minifySources()
    }

    private void exportSources(final File outputDir) {
        if(!exportSources.get()) {
            logger.info("Export JavaScript Sources: [SKIPPED]")
            return
        }

        logger.info("Export JavaScript Sources: ${outputDir}")
        project.copy { CopySpec copy ->
            copy.from(sources)
            copy.into(outputDir)
        }
    }

    private void minifySources() {
        final Set<File> sources = this.sources.get().getFiles()

        if(sources.isEmpty()) {
            logger.info("Compile JavaScript Sources: [SKIPPED]")
        }


        final Set<File> externs = this.externs.get().getFiles() ?: []
        final File compiledOutput = this.minifiedOutput.get()
        final ClosureCompilerExtension compiler = this.compiler.get()
        
        logger.info("Compiling JavaScript...")
        logger.info("""
            sources = ${sources}
            externs = ${externs}
            warningLevel = ${compiler.getWarningLevel()}
            compilationLevel = ${compiler.getCompilationLevel()}
        """.stripIndent())

        // Configure sourcemap if sources are exported
        File sourceMapOutput = null
        if(this.exportSources.get()) {
            sourceMapOutput = this.sourceMapOutput.get()
            compiler.options.setSourceMapOutputPath(sourceMapOutput.path)
            compiler.options.setSourceMapLocationMappings(collectLocationMappings(this.sources.get()))
        }

        final ClosureCompiler closureCompiler = new ClosureCompiler(sources,
                externs,
                compiledOutput,
                sourceMapOutput,
                compiler.getOptions())

        closureCompiler.compile(compiler.getWarningLevel(), compiler.getCompilationLevel())
    }

    /**
     * TODO the location mapping assumes an output directory relative to the document 
     * root, which may not be the case. We need to figure out how to transform the 
     * output to use relative paths from the site base directory
     * @param sourceSet
     * @return
     */
    private List<SourceMap.LocationMapping> collectLocationMappings(final SourceDirectorySet sourceSet) {
        return sourceSet.srcDirs.iterator().collect { File srcDir ->
            new SourceMap.PrefixLocationMapping(srcDir.getAbsolutePath(), "/${outputDir.get().name}")
        }
    }
}
