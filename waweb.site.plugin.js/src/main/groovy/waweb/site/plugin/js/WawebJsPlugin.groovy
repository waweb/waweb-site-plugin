/**
 * 
 */
package waweb.site.plugin.js

import static waweb.site.plugin.Constants.GROUP_NAME
import static waweb.site.plugin.js.Constants.EXTERNS_DISPLAY_NAME
import static waweb.site.plugin.js.Constants.SOURCESET_DISPLAY_NAME

import javax.inject.Inject

import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory

import waweb.site.plugin.DefaultSourceSet
import waweb.site.plugin.SiteExtension
import waweb.site.plugin.SitePluginSpec
import waweb.site.plugin.SourceSet
import waweb.site.plugin.WawebSitePlugin
import waweb.site.plugin.WebComponent

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class WawebJsPlugin implements SitePluginSpec {

    @Delegate
    private Project _target

    private Task _clean

    private Task _assemble


    @Override
    @Inject
    public ProviderFactory getProviderFactory() {}

    void apply(Project project) {
        _target = project

        //ensure base plugin is applied
        getPluginManager().apply(WawebSitePlugin)

        _clean = getTasks().getByName('clean')
        _assemble = getTasks().getByName('assembleSite')

        getExtensions().findByType(SiteExtension).components
                .all(this.&initializeComponentDefaults)
    }

    @Override
    void initializeComponentDefaults(final WebComponent component) {

        // Create a deferred File provider to resolve the default output dir
        final Provider<File> outputDir = providerFactory.provider {
            final File outputDir = getExtensions().findByType(SiteExtension).outputDir
            return outputDir?
                    new File(outputDir, 'js') :
                    getLayout().buildDirectory.dir("${GROUP_NAME}/${component.name}/js").get().asFile
        }

        final JavaScriptSourceSet javascriptSourceSet = component.extend(JavaScriptSourceSet,
                "javascript", SOURCESET_DISPLAY_NAME)
        javascriptSourceSet.srcDir("src/${component.name}/javascript")
        javascriptSourceSet.filter.include("**/*.js")
        javascriptSourceSet.filter.include("**/*.mjs")
        javascriptSourceSet.setOutputDir(outputDir)
        javascriptSourceSet.outputFileName = (component.name == "main") ?
                getProject().name :
                "${getProject().name}-${component.name}"

        SourceSet externsSourceSet =  component.extend(DefaultSourceSet,
                "externs", EXTERNS_DISPLAY_NAME)
        externsSourceSet.srcDir("src/${component.name}/externs")
        externsSourceSet.filter.include("**/*.js")

        // Construct a copy task to export resources to output dir
        final AssembleJs assembleJs = constructAssembleJsTask(component, javascriptSourceSet, externsSourceSet)
        assembleJs.mustRunAfter(_clean)

        // Set task to run in the assemble life-cycle
        _assemble.dependsOn(assembleJs)
    }

    AssembleJs constructAssembleJsTask(final WebComponent component, final JavaScriptSourceSet sources, final SourceDirectorySet externs) {
        final String componentName = (component.name == "main")?
                "" : component.name.capitalize()
        final String taskName = "assembleSite${componentName}Js"

        return getTasks().create(taskName, AssembleJs) { AssembleJs assembleJs ->
            assembleJs.group = GROUP_NAME
            assembleJs.description = "Assemble JavaScript outputs"
            assembleJs.sources.set(sources)
            assembleJs.externs.set(externs)
        }
    }
}
