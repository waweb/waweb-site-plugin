/**
 * 
 */
package waweb.site.plugin.js

import static waweb.site.plugin.js.Constants.DEFAULT_COMPILATION_LEVEL
import static waweb.site.plugin.js.Constants.DEFAULT_LANGUAGE_IN
import static waweb.site.plugin.js.Constants.DEFAULT_LANGUAGE_OUT
import static waweb.site.plugin.js.Constants.DEFAULT_WARNING_LEVEL

import javax.inject.Inject

import org.gradle.api.model.ObjectFactory
import org.gradle.api.tasks.Input

import com.google.javascript.jscomp.CompilerOptions
import com.google.javascript.jscomp.CompilerOptions.LanguageMode

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class ClosureCompilerExtension {

    final CompilerOptions options = new CompilerOptions()

    File minifiedOutput

    File sourceMapOutput

    String warningLevel = DEFAULT_WARNING_LEVEL

    String compilationLevel = DEFAULT_COMPILATION_LEVEL

    String getLanguageIn() {
        return options.getLanguageIn() ?: DEFAULT_LANGUAGE_IN
    }

    void setLanguageIn(String languageIn) {
        final LanguageMode languageMode = languageIn
        options.setLanguageIn(languageMode)
    }

    private String _languageOut

    @Input
    String getLanguageOut() {
        return _languageOut ?: DEFAULT_LANGUAGE_OUT
    }

    void setLanguageOut(String languageOut) {
        final LanguageMode languageMode = languageOut
        options.setLanguageOut(languageMode)
    }

    @Inject
    ObjectFactory getObjectFactory() {}

    ClosureCompilerExtension() {
        this.setLanguageIn(DEFAULT_LANGUAGE_IN)
        this.setLanguageOut(DEFAULT_LANGUAGE_OUT)
    }
}
