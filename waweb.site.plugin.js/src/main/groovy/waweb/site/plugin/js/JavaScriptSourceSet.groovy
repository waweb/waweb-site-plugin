/**
 * 
 */
package waweb.site.plugin.js

import org.gradle.api.internal.file.FileCollectionFactory
import org.gradle.api.internal.file.collections.DirectoryFileTreeFactory
import org.gradle.api.model.ObjectFactory
import org.gradle.api.tasks.util.PatternSet
import org.gradle.internal.Factory

import waweb.site.plugin.DefaultSourceSet
import waweb.site.plugin.WebComponent

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class JavaScriptSourceSet extends DefaultSourceSet {

    final ClosureCompilerExtension compiler

    boolean exportSources = false
    
    String outputFileName

    JavaScriptSourceSet(String name,
            String displayName,
            Factory<PatternSet> patternSetFactory,
            FileCollectionFactory fileCollectionFactory,
            DirectoryFileTreeFactory directoryFileTreeFactory,
            ObjectFactory objectFactory,
            WebComponent component) {
        super(name, displayName, patternSetFactory, fileCollectionFactory, directoryFileTreeFactory, objectFactory, component)

        this.compiler = objectFactory.newInstance(ClosureCompilerExtension)
    }

    void compiler(@DelegatesTo(ClosureCompilerExtension) Closure closure) {
        this.compiler.with(closure)
    }
}
