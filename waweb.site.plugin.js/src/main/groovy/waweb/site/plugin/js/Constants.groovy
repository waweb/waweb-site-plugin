/**
 * 
 */
package waweb.site.plugin.js

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class Constants {

    public static final String SOURCESET_DISPLAY_NAME = "JavaScript Sources"

    public static final String EXTERNS_DISPLAY_NAME = "JavaScript Externs"

    public static final String DEFAULT_COMPILATION_LEVEL = "ADVANCED_OPTIMIZATIONS"

    public static final String DEFAULT_WARNING_LEVEL = "VERBOSE"

    public static final String DEFAULT_LANGUAGE_IN = "ECMASCRIPT_NEXT"

    public static final String DEFAULT_LANGUAGE_OUT = "ECMASCRIPT5_STRICT"
}
