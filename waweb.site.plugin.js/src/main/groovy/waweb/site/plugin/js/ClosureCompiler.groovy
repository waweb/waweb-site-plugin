/**
 * 
 */
package waweb.site.plugin.js

import org.gradle.api.GradleException

import com.google.javascript.jscomp.CommandLineRunner
import com.google.javascript.jscomp.CompilationLevel
import com.google.javascript.jscomp.Compiler
import com.google.javascript.jscomp.CompilerOptions
import com.google.javascript.jscomp.Result
import com.google.javascript.jscomp.SourceFile
import com.google.javascript.jscomp.WarningLevel


/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class ClosureCompiler {

    final List<SourceFile> inputs

    final List<SourceFile> externs

    final File compiledOutput

    final File sourceMapOutput

    final CompilerOptions options

    /**
     * @param source
     * @param externs
     * @param compiledOutput
     * @param sourceMapOutput
     * @param options
     */
    ClosureCompiler(Set<File> source, Set<File> externs, File compiledOutput, File sourceMapOutput, CompilerOptions options) {
        this.compiledOutput = compiledOutput
        this.sourceMapOutput = sourceMapOutput
        this.options = options

        this.inputs = new ArrayList<SourceFile>()
        source.each { File inputFile ->
            this.inputs.add(SourceFile.fromFile(inputFile.getCanonicalPath()))
        }

        this.externs = CommandLineRunner.getBuiltinExterns(new CompilerOptions().getEnvironment())
        this.externs.addAll(externs.collect() { File file ->
            SourceFile.fromFile(file.getCanonicalPath())
        })
    }

    /**
     * @param warningLevel
     * @param compilationLevel
     */
    void compile(final String warningLevel, final String compilationLevel) {

        final Compiler compiler = new Compiler()
        CompilationLevel.valueOf(compilationLevel).setOptionsForCompilationLevel(options)
        WarningLevel level = WarningLevel.valueOf(warningLevel)
        level.setOptionsForWarningLevel(options)

        
        final Result result = compiler.compile(externs, inputs, options)
        if (result.success) {
            compiledOutput.write(compiler.toSource())
            if(sourceMapOutput) {
                def sourceMapContent = new StringBuffer()
                result.sourceMap.appendTo(sourceMapContent, compiledOutput.name)
                sourceMapOutput.write(sourceMapContent.toString())
            }
        }
        else {
            String error = ""
            result.errors.each {
                error += "${it.sourceName}:${it.lineNumber} - ${it.description}\n"
            }
            throw new GradleException(error)
        }
    }
}
