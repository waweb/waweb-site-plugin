@relese=200-beta
@plugin=waweb.site.template
Feature: Enhanced Template Engine
    In order to easily author component-based templates for web
    As a template developer
    I want to apply the "waweb.site.template" plugin
    and use the Groovy Template Engine enhancements
        
Scenario: MAY Render Markdown Content
    Given A new Gradle project 
    And the build content: 
        """
        plugins { id 'waweb.site.template' version '2.0-SNAPSHOT' }
        build { doLast { println "Build Complete." } }
        """
    And the source file "src/main/resources/template/index.groovy": 
        """
        yieldMarkdown '''
        # Heading
        ## Sub-Heading
        - One
        - Two
        - Three
        '''.stripIndent()
        """
    When I envoke the "build" command 
    Then the "build" outcome should be "SUCCESS" 
    And the output should contain "Build Complete." 
    And the build file "build/site/main/index.html" should have text with: 
        """
        <h1>Heading</h1>
        <h2>Sub-Heading</h2>
        <ul>
            <li>One</li>
            <li>Two</li>
            <li>Three</li>
        </ul>
        """

Scenario: MAY Resolve Relative Template Paths
    Given A new Gradle project 
    And the build content: 
        """
        plugins { id 'waweb.site.template' version '2.0-SNAPSHOT' }
        build { doLast { println "Build Complete." } }
        site { 
          components {
            main {
              template { 
                pages {
                    index { title = "Page Title" }
                }
              }
            }
          }
        }
        """
    And the source file "src/main/resources/template/test-layout.groovy": 
        """
        html {
            head { title(title) }
            body { bodyContents() }
        }
        """
    And the source file "src/main/resources/template/test-include.groovy": 
        """
        p('This is the body')
        """
    And the source file "src/main/resources/template/index.groovy": 
        """
        layout 'test-layout.groovy',
            title: page.title, 
            bodyContents: contents { include template: "test-include.groovy" } 
        """
    When I envoke the "build" command 
    Then the "build" outcome should be "SUCCESS" 
    And the output should contain "Build Complete." 
    And the build file "build/site/main/index.html" should have text with: 
        """
        <html>
          <head>
            <title>Page Title</title>
          </head>
          <body>
            <p>This is the body</p>
          </body>
        </html>
        """
