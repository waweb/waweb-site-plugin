@relese=200-beta
@plugin=waweb.site.template
Feature: Typed Checked Templates 
    In order to use type checked models in my templates
    As a web developer
    I want to apply the "waweb.site.template" plugin
    and define the modelTypes in my template
        
Scenario: MAY Render the Default Template Model 
    Given A new Gradle project 
    And the build content: 
        """
        import waweb.site.plugin.template.model.*
        plugins { id 'waweb.site.template' version '2.0-SNAPSHOT' }
        build { doLast { println "Build Complete." } }
        site { 
          components {
            main {
              template { 
                data greeting: "Hello, World!"
                pages {
                    page_01 { 
                      title = "Page 01" 
                      header {
                        title(WaLabel) { text = "Page 1" }
                      }
                      main {
                        title(WaLabel) { text = "Main Heading" }
                        subTitle(WaLabel) { text = "Sub Heading" }
                      }
                    }
                    page_02 { 
                      title = "Page 02" 
                      main(WaText) { content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit." }
                    }
                }
              }
            }
          }
        }
        """
    And the source file "src/main/resources/template/WaPage.groovy": 
        """
        package template
        import waweb.site.plugin.template.model.impl.*
        h1(greeting)
        h2(page.title)
        page.grep(Section) { Section sec ->
            invokeMethod(sec.name) {
                sec.grep(Label, 'title') { h3(it.text) }
                sec.grep(Label, 'subTitle') { h4(it.text) }
            }
        }
        page.grep(Text) { p(it.content) }
        """
    When I envoke the "build" command 
    Then the "build" outcome should be "SUCCESS" 
    And the output should contain "Build Complete." 
    And the build file "build/site/main/page_01.html" should have text with: 
        """
        <h1>Hello, World!</h1>
        <h2>Page 01</h2>
        <header>
          <h3>Page 1</h3>
        </header>
        <main>
          <h3>Main Heading</h3>
          <h4>Sub Heading</h4>
        </main>
        """
    And the build file "build/site/main/page_02.html" should have text with: 
        """
        <h1>Hello, World!</h1>
        <h2>Page 02</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        """

Scenario: MAY Render a Custom Template Model 
    Given A new Gradle project 
    And the build content: 
        """
        import javax.inject.Inject
        import org.gradle.api.model.ObjectFactory
        import waweb.site.plugin.template.model.*
        plugins { id 'waweb.site.template' version '2.0-SNAPSHOT' }
        build { doLast { println "Build Complete." } }

        class Home extends WaPage {
            @Inject
            Home(String name, ObjectFactory objectFactory) {
                super(name, objectFactory)
            }
        }
        class Presentation extends WaPage {
            @Inject
            Presentation(String name, ObjectFactory objectFactory) {
                super(name, objectFactory)
            }
        }

        site { 
          components {
            main {
              template { 
                pages {
                  index(Home) {
                    header {
                      title(WaLabel) { text = "Index" }
                    }
                  }
                  page_01 { 
                    header {
                      title(WaLabel) { text = "Page 01" }
                    }
                    main {
                      title(WaLabel) { text = "Main Heading" }
                      subTitle(WaLabel) { text = "Sub Heading" }
                      text(WaText) { content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit." }
                    }
                  }
                  page_02(Presentation) {
                    header {
                      title(WaLabel) { text = "Page 02" }
                    }
                    main {
                      media(WaImage) { src = "img/test-image.jpeg" }
                    }
                  }
                }
              }
            }
          }
        }
        """
    And the source file "src/main/resources/template/WaPage.groovy": 
        """
        package template
        import waweb.site.plugin.template.model.impl.*
        page.grep(Section) { Section sec ->
            invokeMethod(sec.name) {
                sec.grep(Label, 'title') { h2(it.text) }
                sec.grep(Label, 'subTitle') { h3(it.text) }
            }
        }
        page.grep(Text) { p(it.content) }
        """
    When I envoke the "build" command 
    Then the "build" outcome should be "SUCCESS" 
    And the output should contain "Build Complete." 
    And the build file "build/site/main/index.html" should have text with: 
        """
        <header>
          <h2>Index</h2>
        </header>
        """
    And the build file "build/site/main/page_01.html" should have text with: 
        """
        <header>
          <h2>Page 01</h2>
        </header>
        <main>
          <h2>Main Heading</h2>
          <h3>Sub Heading</h3>
        </main>
        """
    And the build file "build/site/main/page_02.html" should have text with: 
        """
        <header>
          <h2>Page 02</h2>
        </header>
        <main></main>
        """
