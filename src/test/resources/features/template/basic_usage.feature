@relese=200-beta
@plugin=waweb.site.template
Feature: Basic Template Plugin Usage 
    In order to generate HTML pages from a template and model
    As a plugin consumer
    I want to apply the "waweb.site.template" plugin

Scenario: MAY Apply Template Plugin 
    Given A new Gradle project 
    And the build content: 
        """
        plugins { id 'waweb.site.template' version '2.0-SNAPSHOT' }
        build { doLast { println "Build Complete." } }
        """
    When I envoke the "build" command 
    Then the "build" outcome should be "SUCCESS" 
    And the output should contain "Build Complete." 
    
Scenario: MAY Render Simple Template Data
    Given A new Gradle project 
    And the build content: 
        """
        plugins { id 'waweb.site.template' version '2.0-SNAPSHOT' }
        build { doLast { println "Build Complete." } }
        site { 
          components {
            main {
              template {  
                data greeting: "Hello, World!" 
              }
            }
          }
        }
        """
    And the source file "src/main/resources/template/index.groovy": 
        """
        html {
            head { title(greeting) }
            body { p(class: 'para', greeting) }
        }
        """
    When I envoke the "build" command 
    Then the "build" outcome should be "SUCCESS" 
    And the output should contain "Build Complete." 
    And the build file "build/site/main/index.html" should have text with: 
        """
        <html>
          <head><title>Hello, World!</title></head>
          <body><p class="para">Hello, World!</p></body>
        </html>
        """
        