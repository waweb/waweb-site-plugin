@relese=200-beta
@plugin=waweb.site
Feature: Basic Site Plugin Usage 
    In order to build and integrate web based components
    As a plugin consumer
    I want to apply the "waweb.site" plugin

Background: 
    Given A new Gradle project 
    
Scenario: MAY Apply Base Plugin 
    Given the build content: 
        """
    plugins { id 'waweb.site' version '2.0-SNAPSHOT' }
    build { doLast { println "Build Complete." } }
    """
    When I envoke the "build" command 
    Then the "build" outcome should be "SUCCESS" 
    And the output should contain "Build Complete." 
    
    
Scenario: SHOULD assemble static resources in the default output directory 
    Given the build content: 
        """
    plugins { id 'waweb.site' version '2.0-SNAPSHOT' }
    build { doLast { println "Build Complete." } }
    """
    And the source file "src/main/webapp/file1.txt": 
        """
    """ 
    And the source file "src/main/webapp/file2.txt": 
        """
    """
    When I envoke the "build" command 
    Then the "build" outcome should be "SUCCESS" 
    And  the "siteResources" outcome should be "SUCCESS" 
    And  the build file "build/site/main/file1.txt" should exist 
    And  the build file "build/site/main/file2.txt" should exist 
    And the output should contain "Build Complete." 
    
    
Scenario: MAY configure multiple components 
    Given the build content: 
        """
    plugins { id 'waweb.site' version '2.0-SNAPSHOT' }
    site {
      outputDir = file('build/test')
      components {
        main {
          resources {
            outputDir = file('build/main')
          }
        }
        test
      }
    }
    build { doLast { println "Build Complete." } }
    """
    And the source file "src/main/webapp/file1.txt": 
        """
    """ 
    And the source file "src/test/webapp/file2.txt": 
        """
    """
    When I envoke the "build" command 
    Then the "build" outcome should be "SUCCESS" 
    And  the "siteResources" outcome should be "SUCCESS" 
    And  the build file "build/main/file1.txt" should exist 
    And  the build file "build/test/file2.txt" should exist 
    And the output should contain "Build Complete." 
    
