@relese=200-beta
@plugin=waweb.site.js
Feature: Basic JavaScript Plugin Usage
  In order to integrate compiled JavaScript components into my site
  As a plugin consumer
  I want to apply the "waweb.site.js" plugin

  Background: 
    Given A new Gradle project

  Scenario: MAY Apply JavaScript Plugin
    Given the build content:
    """
    plugins { id 'waweb.site.js' version '2.0-SNAPSHOT' }
    build { doLast { println "Build Complete." } }
    """
    When I envoke the "build" command
    Then the "build" outcome should be "SUCCESS"
    And the output should contain "Build Complete."

  Scenario: SHOULD compile default JavaScript component
    Given the settings content:
    """
    rootproject.name = "test-project"
    """
    And the build content:
    """
    plugins { id 'waweb.site.js' version '2.0-SNAPSHOT' }
    build { doLast { println "Build Complete." } }
    """
    And the source file "src/main/javascript/main.js":
    """
    import { greeting } from './greeting.js';
    greeting("Hello, World!");
    """
    And the source file "src/main/javascript/greeting.js":
    """
    export function greeting(message) { console.log(message); }
    """
    When I envoke the "build" command
    Then the "build" outcome should be "SUCCESS"
    And  the "siteJs" outcome should be "SUCCESS"
    And  the output should contain "Build Complete."
    And the build file "build/site/main/js/test-project.min.js" should have text with: 
    """
    'use strict';console.log("Hello, World!");
    """
    And  the build file "build/site/main/js/test-project.sourcemap/json" should not exist
      
  Scenario: MAY export JavaScript sources
    Given the settings content:
    """
    rootproject.name = "test-project"
    """
    And the build content:
    """
    plugins { id 'waweb.site.js' version '2.0-SNAPSHOT' }
    build { doLast { println "Build Complete." } }
    site {
      components {
        main { js { exportSources = true } }
      }
    }
    """
    And the source file "src/main/javascript/main.js":
    """
    import { greeting } from './greeting.js';
    greeting("Hello, World!");
    """
    And the source file "src/main/javascript/greeting.js":
    """
    export function greeting(message) { console.log(message); }
    """
    When I envoke the "build" command
    Then the "build" outcome should be "SUCCESS"
    And  the "siteJs" outcome should be "SUCCESS"
    And  the output should contain "Build Complete."

    And the build file "build/site/main/js/test-project.min.js" should have text with: 
    """
    'use strict';console.log("Hello, World!");
    """
    And the build file "build/site/main/js/test-project.sourcemap.json" should have json with: 
      | version   | 3                                  |
      | file      | "app.min.js"                       |
      | lineCount | 1                                  |
      | sources   | ["/js/greeting.js", "/js/main.js"] |
      | names     | ["console", "log", "message"]      |
    And the build file "build/site/main/js/main.js" should have text with: 
    """
    import { greeting } from './greeting.js';
    greeting("Hello, World!");
    """
    And the build file "build/site/main/js/greeting.js" should have text with: 
    """
    export function greeting(message) { console.log(message); }
    """

  Scenario: MAY configure JavaScript compiler
    Given the settings content:
    """
    rootproject.name = "test-project"
    """
    And the build content:
    """
    plugins { id 'waweb.site.js' version '2.0-SNAPSHOT' }
    build { doLast { println "Build Complete." } }
    """
    And the source file "src/main/javascript/main.js":
    """
    import { greeting } from './greeting.js';
    greeting("Hello, World!");
    """
    And the source file "src/main/javascript/greeting.js":
    """
    export function greeting(message) { console.log(message); }
    """
    When I envoke the "build" command
    Then the "build" outcome should be "SUCCESS"
    And  the "siteJs" outcome should be "SUCCESS"
    And  the output should contain "Build Complete."
    And the build file "build/site/main/js/test-project.min.js" should have text with: 
    """
    'use strict';console.log("Hello, World!");
    """

