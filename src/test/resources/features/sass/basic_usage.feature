@relese=200-beta
@plugin=waweb.site.sass
Feature: Basic Sass Plugin Usage 
    In order to integrate compiled JavaScript components into my site
    As a plugin consumer
    I want to apply the "waweb.site.sass" plugin

Scenario: MAY Apply Sass Plugin 
    Given A new Gradle project 
    And the build content: 
        """
    plugins { id 'waweb.site.sass' version '2.0-SNAPSHOT' }
    build { doLast { println "Build Complete." } }
    """
    When I envoke the "build" command 
    Then the "build" outcome should be "SUCCESS" 
    And the output should contain "Build Complete." 

 Scenario: SHOULD Compile Default Sass Component
    Given A new Gradle project 
    And the build content:
    """
    plugins { id 'waweb.site.sass' version '2.0-SNAPSHOT' }
    build { doLast { println "Build Complete." } }
    """
    And the source file "src/main/sass/main.scss":
    """
    $main_background : #F0F0FF;
    html {
        background: $main_background;
    }
    """
    When I envoke the "build" command
    Then the "build" outcome should be "SUCCESS"
    And  the "siteCss" outcome should be "SUCCESS"
    And  the output should contain "Build Complete."
    And the build file "build/site/main/css/main.min.css" should have text with: 
    """
    @charset "UTF-8";html{background:#f0f0ff}
    """
      
    
