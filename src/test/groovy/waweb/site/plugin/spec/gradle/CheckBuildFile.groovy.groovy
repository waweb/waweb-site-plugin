/**
 * 
 */
package waweb.site.plugin.spec.gradle

import static org.hamcrest.MatcherAssert.assertThat
import static org.hamcrest.Matchers.*
import static org.hamcrest.io.FileMatchers.*
import static org.hamcrest.text.IsEqualCompressingWhiteSpace.equalToCompressingWhiteSpace

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.TaskOutcome

import net.thucydides.core.annotations.Step
import net.thucydides.core.annotations.Steps

import groovy.json.JsonSlurper


/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class CheckBuildFile {

    static final JsonSlurper json = new JsonSlurper()

    @Steps(shared = true)
    CreateGradleProject gradleProject

    File outputFile

    @Step
    void with_path(final String path) {
        this.outputFile = gradleProject.file(path)
    }

    @Step
    void should_exist() {
        assertThat(outputFile, anExistingFile())
    }
    
    @Step
    void should_not_exist() {
        assertThat(outputFile, not(anExistingFile()))
    }

    @Step
    void should_have_text(final String content) {
        assertThat(outputFile.text.replaceAll(/\s+/, ''), 
                equalToCompressingWhiteSpace(content.replaceAll(/\s+/, '')))
    }

    @Step
    void should_have_json_entries(final Map expectedEntries) {
        final Map jsonContents = json.parseText(outputFile.text)
        expectedEntries.each { key, value ->
            //TODO use chainining to display all results
            assertThat(jsonContents, hasEntry(key, value))
        }
    }
}
