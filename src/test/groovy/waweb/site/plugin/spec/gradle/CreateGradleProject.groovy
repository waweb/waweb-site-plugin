/**
 * 
 */
package waweb.site.plugin.spec.gradle

import static org.hamcrest.MatcherAssert.assertThat
import static org.hamcrest.Matchers.*

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.junit.rules.TemporaryFolder

import net.thucydides.core.annotations.Step

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class CreateGradleProject {

    TemporaryFolder tempDir
    
    GradleRunner runner

    File buildScriptFile
    
    File settingsFile
    
    File getProjectDir() {
        return this.tempDir.getRoot()
    }
    
    File file(final String path) {
        return new File(this.getProjectDir(), path)
    }


    @Step
    void with_project_dir(final TemporaryFolder tempDir) {
        this.tempDir = tempDir
        
        this.buildScriptFile = tempDir.newFile('build.gradle')
        
        settingsFile = tempDir.newFile('settings.gradle')
        
        final URL pluginClasspathResource = getClass().classLoader.findResource("plugin-classpath.txt")
        if (pluginClasspathResource == null) {
            throw new IllegalStateException("plugin-classpath.txt is not included in runtimeclasspath")
        }

        final List<File> pluginClasspath = pluginClasspathResource
                .readLines()
                .collect { new File(it) }
                
        this.runner = GradleRunner.create()
                .withProjectDir(this.getProjectDir())
                .withPluginClasspath(pluginClasspath)
    }

    @Step
    void with_build_content(String content) {
        buildScriptFile.append(content, 'UTF-8')
    }
    
    @Step
    void with_settings_content(String content) {
        settingsFile.append(content, 'UTF-8')
    }

}
