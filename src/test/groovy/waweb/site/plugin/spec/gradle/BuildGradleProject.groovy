/**
 * 
 */
package waweb.site.plugin.spec.gradle

import static org.hamcrest.MatcherAssert.assertThat
import static org.hamcrest.Matchers.*
import static org.hamcrest.io.FileMatchers.*
import static org.hamcrest.text.IsEqualIgnoringWhiteSpace.equalToIgnoringWhiteSpace

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.TaskOutcome

import net.thucydides.core.annotations.Step
import net.thucydides.core.annotations.Steps

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class BuildGradleProject {

    @Steps(shared = true)
    CreateGradleProject gradleProject

    BuildResult result

    @Step
    void with_source_file(String path, String content) {
        def file = new File(gradleProject.getProjectDir(), path)
        file.parentFile.mkdirs()
        file.text = content
    }

    @Step
    void run_build_with_arguments(List<String> args) {
        result = gradleProject.runner
                .withArguments(args)
                .build()
    }

    @Step
    void task_outcome_should_be(String taskName, String taskOutcomeName) {
        final TaskOutcome taskOutcome = taskOutcomeName.toUpperCase()
        assertThat(result.task(":${taskName}").outcome,
                equalTo(taskOutcome))
    }

    @Step
    void output_should_contain(String text) {
        assertThat(result.output, containsString(text))
    }
}
