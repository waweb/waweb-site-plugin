
package waweb.site.plugin.spec.steps

import org.junit.Rule
import org.junit.rules.TemporaryFolder

import cucumber.api.java.After
import cucumber.api.java.Before
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import net.thucydides.core.annotations.Steps
import waweb.site.plugin.spec.gradle.BuildGradleProject
import waweb.site.plugin.spec.gradle.CreateGradleProject
import waweb.site.plugin.spec.gradle.CheckBuildFile

import groovy.util.Eval


class GradleStepDefinitions {
    
    @Rule
    TemporaryFolder tempDir = new TemporaryFolder()
    
    @Steps(shared = true)
    CreateGradleProject gradleProject
    
    @Steps(shared = true)
    BuildGradleProject gradleBuild

    @Steps
    CheckBuildFile buildOutput
    
    @Before
    void before() {
        tempDir.create()
    }
    
    @After
    void after() {
        tempDir.delete()
    }
    
    @Given('^A new Gradle project$')
    void a_new_gradle_project() {
        gradleProject.with_project_dir(tempDir)
    }

    @Given('^the settings content:$')
    void the_settings_content(String content) {
        gradleProject.with_settings_content(content)
    }
    
    @Given('^the build content:$')
    void the_build_content(String content) {
        gradleProject.with_build_content(content)
    }

    @Given('^the source file "([^"]*)":$')
    void the_source_file(String path, String content) {
        gradleBuild.with_source_file(path, content)
    }

    @Given('^the source files:$')
    void the_source_files(List<Map<String, String>> sources) {
        sources.each { gradleBuild.with_source_file(it.path, it.content) }
    }

    @When('^I envoke the "([^"]*)" command$')
    void i_envoke_the_command(String arg) {
        gradleBuild.run_build_with_arguments([arg])
    }

    @Then('^the "([^"]*)" outcome should be "([^"]*)"$')
    void the_outcome_should_be(String taskName, String taskOutcome) {
        gradleBuild.task_outcome_should_be(taskName, taskOutcome)
    }

    @Then('^the output should contain "([^"]*)"$')
    void the_output_should_contain(String text) {
        gradleBuild.output_should_contain(text)
    }

    @Then('^the build file "([^"]*)" should exist$')
    void the_build_file_should_exist(String path) {
        buildOutput.with_path(path)
        buildOutput.should_exist()
    }
    
    @Then('^the build file "([^"]*)" should not exist$')
    void the_build_file_should_not_exist(String path) {
        buildOutput.with_path(path)
        buildOutput.should_not_exist()
    }
    
    @Then('^the build file "([^"]*)" should have text with:$')
    void the_build_file_should_have_text_with(String path, String content) {
        buildOutput.with_path(path)
        buildOutput.should_exist()
        buildOutput.should_have_text(content)
    }

    @Then('^the build file "([^"]*)" should have json with:$')
    public void the_build_file_have_json_with(String path, Map<String, String> entries) {
        buildOutput.with_path(path)
        buildOutput.should_exist()
        buildOutput.should_have_json_entries(entries.collectEntries { k, v -> [(k): Eval.me(v)] })
    }

    @Then('^the build files should exist:$')
    void the_build_files_should_exist(List<String> paths) {
        paths.each { path ->
            buildOutput.with_path(path)
            buildOutput.should_exist()
        }
    }
}
