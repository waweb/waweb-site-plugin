/**
 * 
 */
package waweb.site.plugin.template.model.impl

import groovy.transform.Canonical

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
@Canonical
class Label implements ModelMemento {
    String name
    String text
}
