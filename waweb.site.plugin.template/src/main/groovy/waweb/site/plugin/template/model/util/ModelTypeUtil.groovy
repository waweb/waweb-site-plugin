/**
 * 
 */
package waweb.site.plugin.template.model.util

import org.codehaus.groovy.reflection.CachedClass

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class ModelTypeUtil {

    static Set<String> listTypes(final Object instance) {
        return instance.metaClass.getSuperClasses()
                .drop(1)
                .reverse()
                .drop(1)
                .collect { CachedClass cls -> cls.theClass.simpleName }
    }
}
