/**
 * 
 */
package waweb.site.plugin.template.engine

import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.InputFiles

import groovy.transform.Canonical

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
@Canonical
class TemplateFileCollection implements Serializable {
    
    @InputDirectory
    File rootDirectory
    
    @InputFiles
    Set<File> files
}
