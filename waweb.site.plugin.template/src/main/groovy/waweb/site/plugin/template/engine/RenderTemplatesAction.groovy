/**
 * 
 */
package waweb.site.plugin.template.engine

import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.workers.WorkAction

import groovy.text.markup.MarkupTemplateEngine
import groovy.text.markup.TemplateConfiguration
import waweb.site.plugin.template.WaPageTemplate

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
abstract class RenderTemplatesAction implements WorkAction<RenderTemplatesParameters> {

    @Override
    void execute() {

        //TODO use task inputs
        final TemplateConfiguration config = new TemplateConfiguration()
        config.declarationEncoding = 'UTF-8'
        config.expandEmptyElements = true
        config.useDoubleQuotes = true
        config.autoIndent = true
        config.autoNewLine = true
        config.baseTemplateClass = WaPageTemplate

        final ClassLoader classLoader = Thread.currentThread().getContextClassLoader()
        final Set<TemplateFileCollection> templates = getParameters().getTemplates().get()

        final MarkupTemplateEngine markupEngine = new MarkupTemplateEngine(classLoader, config,
                new DefaultTemplateResolver(templates))

        final File templateFile = getParameters().getInputFile().get().asFile
        final File outputFile = getParameters().getOutputFile().get().asFile
        final Map model = getParameters().getModel().get()

        outputFile.parentFile.mkdirs()
        outputFile.withWriter { Writer writer ->
            templateFile.withReader { Reader reader ->
                markupEngine.createTemplate(reader)
                        .make(model).writeTo(writer)
            }
        }
    }
}
