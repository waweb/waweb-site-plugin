/**
 * 
 */
package waweb.site.plugin.template.model.impl

import groovy.transform.Canonical

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
@Canonical
class Image implements ModelMemento {
    String name
    String label
    String src
}
