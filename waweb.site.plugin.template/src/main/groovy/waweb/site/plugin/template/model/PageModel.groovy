/**
 * 
 */
package waweb.site.plugin.template.model

import org.gradle.api.Named
import org.gradle.api.provider.MapProperty

import waweb.site.plugin.template.model.impl.Page

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
interface PageModel<T extends Page> extends Named {
    MapProperty<String, Object> getData()
    String getTitle()
    void data(final Map entries)
    T toMemento()
}
