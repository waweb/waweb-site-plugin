/**
 * 
 */
package waweb.site.plugin.template

import static waweb.site.plugin.Constants.GROUP_NAME

import javax.inject.Inject

import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.plugins.GroovyBasePlugin
import org.gradle.api.plugins.JavaBasePlugin
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory

import waweb.site.plugin.SiteExtension
import waweb.site.plugin.SitePluginSpec
import waweb.site.plugin.WawebSitePlugin
import waweb.site.plugin.WebComponent

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class WawebTemplatePlugin implements SitePluginSpec  {

    @Delegate
    private Project _target

    private Task _clean

    private Task _assemble

    @Override
    @Inject
    ProviderFactory getProviderFactory() {}

    @Override
    void apply(Project project) {
        _target = project

        // Ensure base plugins are present
        getPluginManager().apply(JavaBasePlugin)
        getPluginManager().apply(GroovyBasePlugin)
        getPluginManager().apply(WawebSitePlugin)

        // Hold a reference to some life-cycle tasks
        _clean = getTasks().getByName('clean')
        _assemble = getTasks().getByName('assembleSite')

        // Ectend each component with a template SourceSet
        getExtensions().findByType(SiteExtension)
                .components.all(this.&initializeComponentDefaults)
    }

    @Override
    void initializeComponentDefaults(final WebComponent component) {
        // Create a deferred File provider to resolve the default output dir
        final Provider<File> outputDir = providerFactory.provider {
            final File outputDir = getExtensions().findByType(SiteExtension).outputDir
            return outputDir ?:
                    getLayout().buildDirectory.dir("${GROUP_NAME}/${component.name}").get().asFile
        }

        // Initialize template source set defaults
        final TemplateSourceSet sourceSet = component.extend(TemplateSourceSet,
                "template", "Template Sources")
        sourceSet.srcDir("src/${component.name}/resources/template")
        sourceSet.setOutputDir(outputDir)

        // Construct a task to export templates to output dir
        final RenderTemplates renderTemplates = constructRenderTemplatesTask(component, sourceSet)
        renderTemplates.mustRunAfter(_clean)

        // Set task to run in the assemble life-cycle
        _assemble.dependsOn(renderTemplates)
    }

    RenderTemplates constructRenderTemplatesTask(final WebComponent component, final TemplateSourceSet templates) {
        final String componentName = (component.name == "main")?
                "" : component.name.capitalize()
        final String taskName = "assembleSite${componentName}Templates"

        return getTasks().create(taskName, RenderTemplates) { RenderTemplates renderTemplates ->
            renderTemplates.group = GROUP_NAME
            renderTemplates.description = "Assemble Template Outputs"
            renderTemplates.templates.set(templates)
        }
    }
}
