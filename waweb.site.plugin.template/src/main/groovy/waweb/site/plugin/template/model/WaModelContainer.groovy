/**
 * 
 */
package waweb.site.plugin.template.model

import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.model.ObjectFactory
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Nested
import org.gradle.api.tasks.Optional

import waweb.site.plugin.template.model.impl.ModelMemento

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
abstract class WaModelContainer<T extends ModelMemento> extends WaModel<T> {

    @Internal
    Class<?> defaultElementType

    @Nested
    @Optional
    final NamedDomainObjectContainer<WaModel> elements

    WaModelContainer(String name, ObjectFactory objectFactory) {
        super(name, objectFactory)
        this.elements = objectFactory.domainObjectContainer(WaModel)
    }

    Object methodMissing(String name, Object args) {
        // println "${this}.methodMissing(${name}, ${args})"

        if(((List) args).size() == 0) {
            // Create default instance of default type
            if(!defaultElementType) throw new RuntimeException("Default Type Required.")
            elements.add(objectFactory.newInstance(defaultElementType, name))
        }
        else if(((List) args).size() == 1) {
            if(args.getAt(0) instanceof Closure) {
                // Configure or create an instance with the provided closure
                final T instance = elements.findByName(name)
                if(instance) {
                    // Configure instance if it exists
                    instance.with(closure)
                }
                else {
                    // Otherwise Create and configure instance of default type
                    if(!defaultElementType) throw new RuntimeException("Default Type Required.")
                    elements.add(objectFactory.newInstance(defaultElementType, name).tap(args.getAt(0)))
                }
            }
            else {
                // Otherwise create default instance of specified type
                final Class<T> type = args.getAt(0)
                elements.add(objectFactory.newInstance(type, name))
            }
        }
        else if(((List) args).size() == 2) {
            // Create and configure instance of specified type
            final Class<T> type = args.getAt(0)
            final Closure config = args.getAt(1)
            elements.add(objectFactory.newInstance(type, name).tap(config))
        }
    }
}