/**
 * 
 */
package waweb.site.plugin.template.model

import javax.inject.Inject

import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.MapProperty
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional

import waweb.site.plugin.template.model.impl.Page

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class WaPage extends WaModelContainer<Page> implements PageModel<Page> {

    @Input
    @Optional
    final MapProperty<String, Object> data

    @Input
    @Optional
    String title

    @Inject
    WaPage(String name, ObjectFactory objectFactory) {
        super(name, objectFactory)
        this.data = objectFactory.mapProperty(String, Object)
        this.defaultElementType = WaSection
    }

    void data(final Map entries) {
        this.data.putAll(entries)
    }

    Page toMemento() {
        return new Page(
            data: this.data.get(),
            elements: this.elements.collect { WaModel node -> node.toMemento() },
            name: this.name,
            title: this.title
        )
    }
}