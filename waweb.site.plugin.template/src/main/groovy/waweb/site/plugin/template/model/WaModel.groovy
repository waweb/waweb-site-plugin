/**
 * 
 */
package waweb.site.plugin.template.model

import static waweb.site.plugin.template.model.util.ModelTypeUtil.listTypes

import org.gradle.api.Named
import org.gradle.api.model.ObjectFactory
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal

import waweb.site.plugin.template.model.impl.ModelMemento

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
abstract class WaModel<T extends ModelMemento> implements Named {

    @Internal
    final ObjectFactory objectFactory

    @Input
    final String name

    @Internal
    Set<String> getTypes() {
        return listTypes(this)
    }

    WaModel(String name, ObjectFactory objectFactory) {
        this.name = name
        this.objectFactory = objectFactory
    }

    abstract T toMemento()


    String toString() {
        return "${this.class.name}(${this.name})"
    }
}