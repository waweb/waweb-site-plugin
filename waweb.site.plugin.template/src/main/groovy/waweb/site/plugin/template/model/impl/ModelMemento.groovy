/**
 * 
 */
package waweb.site.plugin.template.model.impl

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
interface ModelMemento extends Serializable {
    abstract String getName()
}
