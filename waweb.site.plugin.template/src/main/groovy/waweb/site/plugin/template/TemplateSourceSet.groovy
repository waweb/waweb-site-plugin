/**
 * 
 */
package waweb.site.plugin.template

import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.internal.file.FileCollectionFactory
import org.gradle.api.internal.file.collections.DirectoryFileTreeFactory
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.MapProperty
import org.gradle.api.tasks.util.PatternSet
import org.gradle.internal.Factory

import waweb.site.plugin.DefaultSourceSet
import waweb.site.plugin.WebComponent
import waweb.site.plugin.template.model.PageModel
import waweb.site.plugin.template.model.WaPage
import waweb.site.plugin.util.NamedDomainObjectFactory

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class TemplateSourceSet extends DefaultSourceSet {

    final MapProperty<String, Object> data

    final NamedDomainObjectContainer<PageModel> pages

    final NamedDomainObjectFactory<PageModel> pageFactory

    public TemplateSourceSet(String name,
    String displayName,
    Factory<PatternSet> patternSetFactory,
    FileCollectionFactory fileCollectionFactory,
    DirectoryFileTreeFactory directoryFileTreeFactory,
    ObjectFactory objectFactory,
    WebComponent component) {
        super(name, displayName, patternSetFactory, fileCollectionFactory, directoryFileTreeFactory, objectFactory, component)

        this.data = objectFactory.mapProperty(String, Object)

        this.pages = objectFactory.domainObjectContainer(PageModel)
        this.pageFactory = new NamedDomainObjectFactory<PageModel>(objectFactory, this.pages, WaPage)
    }

    void data(final Map entries) {
        this.data.putAll(entries)
    }

    void pages(final Closure closure) {
        this.pageFactory.createOrConfigureAll(closure)
    }
}