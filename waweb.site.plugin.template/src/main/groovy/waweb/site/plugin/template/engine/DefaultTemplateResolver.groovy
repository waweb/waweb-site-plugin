/**
 * 
 */
package waweb.site.plugin.template.engine

import java.nio.file.FileSystems
import java.nio.file.Path

import groovy.text.markup.MarkupTemplateEngine
import groovy.text.markup.TemplateConfiguration
import groovy.text.markup.TemplateResolver

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class DefaultTemplateResolver implements TemplateResolver {
    
    private TemplateConfiguration templateConfiguration
    
    private ClassLoader templateClassLoader
    
    private final Set<TemplateFileCollection> templates

    public DefaultTemplateResolver(Set<TemplateFileCollection> templates) {
        this.templates = templates
    }

    @Override
    public void configure(final ClassLoader templateClassLoader, final TemplateConfiguration configuration) {
        this.templateClassLoader = templateClassLoader
        this.templateConfiguration = configuration
    }

    @Override
    public URL resolveTemplate(final String templatePath) throws IOException {
        
        final MarkupTemplateEngine.TemplateResource templateResource = MarkupTemplateEngine.TemplateResource.parse(templatePath)
        final String configurationLocale = templateConfiguration.getLocale().toString().replace("-", "_")
        
        // Search the templates for a file that matches the ending segment
        // Each template set is searched, selecting the last valid match, if any
        final Path templateResourcePath = FileSystems.getDefault().getPath(templateResource.withLocale(configurationLocale).toString())
        final Path templateResourceLocalePath = FileSystems.getDefault().getPath(templateResource.withLocale(null).toString())
        
        File templateFile = null
        this.templates.each { TemplateFileCollection templateSet ->
            templateFile = templateSet.files.find { File sourceFile ->
                final Path relativePath = templateSet.rootDirectory.toPath().relativize(sourceFile.toPath())
                return relativePath.equals(templateResourceLocalePath) || 
                        relativePath.equals(templateResourcePath)
            }
        }

        URL resource = templateFile? templateFile.toURI().toURL() : null
        
        if (resource == null) {
            resource = templateResource.hasLocale() ? templateClassLoader.getResource(templateResource.toString()) : null
        }
        if (resource == null) {
            // no explicit locale in the template path or resource not found
            // fallback to the default configuration locale
            resource = templateClassLoader.getResource(templateResource.withLocale(configurationLocale).toString())
        }
        if (resource == null) {
            // no resource found with the default locale, try without any locale
            resource = templateClassLoader.getResource(templateResource.withLocale(null).toString())
        }
        if (resource == null) {
            throw new IOException("Unable to load template:" + templatePath)
        }
        
        return resource
    }
}
