/**
 * 
 */
package waweb.site.plugin.template

import com.vladsch.flexmark.html.HtmlRenderer
import com.vladsch.flexmark.parser.Parser

import groovy.text.markup.BaseTemplate
import groovy.text.markup.MarkupTemplateEngine
import groovy.text.markup.TemplateConfiguration
import waweb.site.plugin.template.model.impl.ModelMemento

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
abstract class WaPageTemplate extends BaseTemplate {

    static final Parser markdownParser = Parser.builder().build()

    private final Map<String,String> modelTemplates = [:]

    public void modelTemplates(final Map entries) {
        modelTemplates.putAll(entries)
    }

    public String resolveTemplateFor(final ModelMemento model) {
        // println "resolveTemplateFor(${model})"
        return modelTemplates.get(model.name)
    }

    /**
     * @param templateEngine
     * @param model
     * @param modelTypes
     * @param configuration
     */
    public WaPageTemplate(MarkupTemplateEngine templateEngine, Map model, Map<String, String> modelTypes, TemplateConfiguration configuration) {
        super(templateEngine, model, modelTypes, configuration)
    }

    /**
     * Renders the markdown String content as html, and writes it directly to the output
     * @param content
     * @return
     * @throws IOException
     */
    public WaPageTemplate yieldMarkdown(final String content) throws IOException {
        if(!content) return this

        HtmlRenderer.builder().build()
                .render(markdownParser.parse(content), this.getOut())

        return this
    }

    public Object methodMissing(String tagName, Object args) throws IOException {
        // println "methodMissing(${tagName}, ${args})"
        return super.methodMissing(tagName, args)
    }
}
