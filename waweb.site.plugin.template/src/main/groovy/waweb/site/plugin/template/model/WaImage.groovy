/**
 * 
 */
package waweb.site.plugin.template.model


import javax.inject.Inject

import org.gradle.api.model.ObjectFactory
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional

import waweb.site.plugin.template.model.impl.Image

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class WaImage extends WaModel<Image> {

    @Input
    String src
    
    @Optional
    @Input
    String label

    @Inject
    WaImage(String name, ObjectFactory objectFactory) {
        super(name, objectFactory)
    }

    Image toMemento() {
        return new Image(
            name: this.name,
            label: this.label,
            src: this.src
        )
    }
}