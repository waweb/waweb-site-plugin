/**
 * 
 */
package waweb.site.plugin.template

import java.nio.file.Path

import javax.inject.Inject

import org.gradle.api.DefaultTask
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Nested
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import org.gradle.workers.WorkQueue
import org.gradle.workers.WorkerExecutor

import waweb.site.plugin.template.engine.DefaultTemplateResolver
import waweb.site.plugin.template.engine.RenderTemplatesAction
import waweb.site.plugin.template.engine.RenderTemplatesParameters
import waweb.site.plugin.template.engine.TemplateFileCollection
import waweb.site.plugin.template.model.PageModel
import waweb.site.plugin.template.model.WaModel

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class RenderTemplates extends DefaultTask {

    @Input
    final Property<String> encoding = objectFactory.property(String)

    @Optional
    @Input
    final Provider<Map> data = providerFactory.provider {
        return this.templates.get().data.get()
    }

    @Nested
    final Provider<Collection<PageModel>> pages = providerFactory.provider {
        return this.templates.get().pages
    }

    @InputFiles
    final Property<TemplateSourceSet> templates = objectFactory.property(TemplateSourceSet)

    @OutputDirectory
    final Provider<File> outputDir = providerFactory.provider {
        return templates.get().getOutputDir()
    }

    @Inject
    ObjectFactory getObjectFactory() {}

    @Inject
    ProviderFactory getProviderFactory() {}

    @Inject
    WorkerExecutor getWorkerExecutor() {}


    RenderTemplates() {
        this.encoding.convention(System.properties.get('file.encoding'))
    }

    @TaskAction
    void run() {

        final TemplateSourceSet templateSourceSet = this.templates.get()
        final File outputDir = this.outputDir.get()
        final Collection<PageModel> pages = this.pages.get()
        final WorkQueue workQueue = workerExecutor.noIsolation()
        
        final Set<TemplateFileCollection> templates = templateSourceSet.srcDirs.collect { File rootDir ->
            //TODO remove uneeded iterations
            return new TemplateFileCollection(rootDirectory: rootDir, files: templateSourceSet.files.findAll { File file ->
                file.toPath().startsWith(rootDir.toPath())
            })
        }
        
        if(pages.isEmpty()) {
            logger.info("Rendering Templates At: ${outputDir}")
            renderBasicTemplates(workQueue, templates, outputDir, this.data.get())
        }
        else {
            logger.info("Rendering Type Checked Templates At: ${outputDir}")
            renderTypedTemplates(workQueue, templates, outputDir, this.data.get(), pages)
        }
    }

    void renderTypedTemplates(final WorkQueue workQueue, final Set<TemplateFileCollection> templates, final File outputDir, final Map data, final Collection<PageModel> pages) {
        
        pages.each { PageModel page ->
            final Map model = [ *: data, page: page.toMemento() ]
            final File outputFile = new File(outputDir, "${page.name}.html")
            final File templateFile = resolveTemplate(templates, page)

            if(!templateFile || !templateFile.exists()) {
                throw new Exception("Unable to resolve template by name (${page.name}) or type (${page.class.name}).")
            }

            logger.info("Rendering ${page.class.name}(${page.name}): ${templateFile} => ${outputFile}")
            workQueue.submit(RenderTemplatesAction) { RenderTemplatesParameters parameters ->
                parameters.getModel().set(model)
                parameters.getOutputFile().set(outputFile)
                parameters.getInputFile().set(templateFile)
                parameters.getTemplates().set(templates)
            }
        }
    }

    void renderBasicTemplates(final WorkQueue workQueue, final Set<TemplateFileCollection> templates, final File outputDir, final Map model) {
        // Itterate over each template file, rendering it with the provided model
        templates.each { TemplateFileCollection templateSet ->
            templateSet.files.each { File file ->
                final Path relativePath = templateSet.rootDirectory.toPath().relativize(file.toPath())
                final String basePath = relativePath.toString()
                        .take(relativePath.toString().lastIndexOf('.'))
                final File outputFile = new File(outputDir, "${basePath}.html")
                logger.info("Rendering: ${relativePath} => ${outputFile}")
                workQueue.submit(RenderTemplatesAction) { RenderTemplatesParameters parameters ->
                    parameters.getModel().set(model)
                    parameters.getOutputFile().set(outputFile)
                    parameters.getInputFile().set(file)
                    parameters.getTemplates().set(templates)
                }
            }
        }
    }

    File resolveTemplate(final Set<TemplateFileCollection> templates, final WaModel model) {
        
        // Check for file name match first, and select the last found if any
        File templateFile = null
        templates.each { TemplateFileCollection templateSet ->
            templateFile = templateSet.files.find { File file ->
                final Path relativePath = templateSet.rootDirectory.toPath().relativize(file.toPath())
                return "${model.getName()}.groovy" == relativePath.toString()
            }
        }

        // Resolve template by name if found
        if(templateFile) {
            return templateFile
        }
        
        // Check for a match by type name in the inheritance chain, returning the last found if any
        final Set<String> types = model.getTypes()
        templates.each { TemplateFileCollection templateSet ->
            templateFile = templateSet.files.find { File file ->
                final Path relativePath = templateSet.rootDirectory.toPath().relativize(file.toPath())
                return types.any { String typeName ->
                    return "${typeName}.groovy" == relativePath.toString()
                    
                }
            }
        }

        return templateFile
    }
}
