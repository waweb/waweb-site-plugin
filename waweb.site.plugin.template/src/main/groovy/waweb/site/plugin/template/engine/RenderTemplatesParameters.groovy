/**
 * 
 */
package waweb.site.plugin.template.engine

import org.gradle.api.file.RegularFileProperty
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.SetProperty
import org.gradle.workers.WorkParameters

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
interface RenderTemplatesParameters extends WorkParameters {
    MapProperty<String, Object> getModel()
    RegularFileProperty getInputFile()
    RegularFileProperty getOutputFile()
    SetProperty<TemplateFileCollection> getTemplates()
}
