/**
 * 
 */
package waweb.site.plugin.template.model

import javax.inject.Inject

import org.gradle.api.model.ObjectFactory
import org.gradle.api.tasks.Input

import waweb.site.plugin.template.model.impl.Label

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class WaLabel extends WaModel<Label> {

    @Input
    String text

    @Inject
    WaLabel(String name, ObjectFactory objectFactory) {
        super(name, objectFactory)
    }

    Label toMemento() {
        return new Label(
            name: this.name,
            text: this.text
        )
    }
}