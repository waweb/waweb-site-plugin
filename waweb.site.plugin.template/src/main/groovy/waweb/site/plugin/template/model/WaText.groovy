/**
 * 
 */
package waweb.site.plugin.template.model

import javax.inject.Inject

import org.gradle.api.model.ObjectFactory
import org.gradle.api.tasks.Input

import waweb.site.plugin.template.model.impl.Text

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class WaText extends WaModel<Text> {

    @Input
    String content

    @Inject
    WaText(String name, ObjectFactory objectFactory) {
        super(name, objectFactory)
    }

    Text toMemento() {
        return new Text(
            name: this.name,
            content: this.content
        )
    }
}