/**
 * 
 */
package waweb.site.plugin.template.model.impl

import groovy.transform.Canonical

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
@Canonical
class Section implements ModelMemento {
    Map data
    List<ModelMemento> elements
    String name
    String title

    public <T extends ModelMemento> void grep(final Class<T> type, final Closure action) {
        this.elements.findAll({ it in type }).each(action)
    }

    public <T extends ModelMemento> void grep(final Class<T> type, final String name, final Closure action) {
        this.elements.findAll({ it in type && it.name == name }).each(action)
    }
}
