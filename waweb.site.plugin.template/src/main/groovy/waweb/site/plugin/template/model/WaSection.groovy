/**
 * 
 */
package waweb.site.plugin.template.model

import javax.inject.Inject

import org.gradle.api.model.ObjectFactory
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional

import waweb.site.plugin.template.model.impl.Section

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class WaSection extends WaModelContainer<Section> {

    @Input
    @Optional
    String title

    @Inject
    WaSection(String name, ObjectFactory objectFactory) {
        super(name, objectFactory)

        this.defaultElementType = WaSection
    }

    Section toMemento() {
        return new Section(
            elements: this.elements.collect { WaModel node -> node.toMemento() },
            name: this.name,
            title: this.title
        )
    }
}